package Zadania;

import java.util.Scanner;

public class CorrectPin {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String correctPassword = "newyork";

        int count = 0;
        String userInput;
        do {
            System.out.println("Write a password:");
            userInput = scanner.next();
            if (userInput.equals(correctPassword)) {
                System.out.println("Password is correct! You are in!");
                break;
            } else
                System.out.println("Password is incorrect.");
            count++;

            if (count == 3) System.out.println("You tried too many times. Try again in half hour.");
        } while (count < 3);
    }
}
